export default {
    data: () => ({
        Teste: "Teste",
        TesteBackup: "",
    }),
    computed: {
        modal: {
            get() {
                return this.Teste;
            },
            set(newValue, oldValue) {
                this.Teste = newValue;
                this.TesteBackup = oldValue;
            }
        }
    },
    methods: {
        alertarDeTeste() {
            alert(this.Teste);
        }
    }
}